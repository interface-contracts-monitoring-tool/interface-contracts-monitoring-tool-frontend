FROM python:3.8-buster
WORKDIR /app
COPY ./requirements.txt /app
RUN /usr/local/bin/python -m pip install --upgrade pip && pip install -r requirements.txt
RUN apt-get update && \
    apt-get install -y apt-transport-https ca-certificates wget dirmngr gnupg software-properties-common && \
    wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - && \
    add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ && \
    apt-get update && \
    apt-get install -y adoptopenjdk-8-hotspot
COPY . /app
ENTRYPOINT python src/wsgi.py --api_host=$API_HOST --mount_point=$MOUNT_POINT
