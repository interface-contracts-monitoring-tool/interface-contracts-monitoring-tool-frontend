import os
import tempfile
import shutil
import subprocess
from typing import Tuple


def calc_changelog(old:str, new:str):

    # with open("test/old.yaml", "r") as fp:
    #     old = fp.read()
    # with open("test/new.yaml", "r") as fp:
    #     new = fp.read()

    tmpdir_old = tempfile.mkdtemp()
    tmpdir_new = tempfile.mkdtemp()
    tmpdir_res = tempfile.mkdtemp()

    filename_old = os.path.join(tmpdir_old, 'old.yaml')
    filename_new = os.path.join(tmpdir_new, 'new.yaml')
    filename_res = os.path.join(tmpdir_res, 'res.html')

    os.mkfifo(filename_old)
    os.mkfifo(filename_new)
    os.mkfifo(filename_res)

    result = None
    try:
        jar_path = "src/changelog/openapi-diff.jar"

        cmd = ['java', '-jar', jar_path, filename_old, filename_new, '--html', filename_res]

        process = subprocess.Popen(cmd,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE
                        )
        # stdout, stderr = process.communicate('S\nL\n')[0]
        # print(stdout.decode("utf-8"))
        # print(stderr.decode("utf-8"))

        with open(filename_old, "w") as fp:
            fp.write(old)
        with open(filename_new, "w") as fp:
            fp.write(new)
        with open(filename_res, "r") as fp:
            result = fp.read()
    finally:
        shutil.rmtree(tmpdir_old)
        shutil.rmtree(tmpdir_new)
        shutil.rmtree(tmpdir_res)
    result = result.replace("http://deepoove.com/swagger-diff/stylesheets/demo.css", "/icmt/diff.css")
    print(result)
    return result
