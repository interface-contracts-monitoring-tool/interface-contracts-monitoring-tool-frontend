from flask import Flask, render_template, request
from jinja2 import escape
from flask_paginate import Pagination, get_page_parameter
from requests import get
from dateutil.parser import isoparse
from copy import copy
from changelog import calc_changelog
import sys
from werkzeug.routing import Rule
import argparse
from flask_oidc import OpenIDConnect


parser = argparse.ArgumentParser()
parser.add_argument("--api_host", help="Host the backend is running on", type=str, default="ibm-intelling.polito.it:4443/efpf/icmt-backend")
parser.add_argument("--mount_point", help="Mount point for the ICMT webapp instance", type=str, default="icmtdev")
args = parser.parse_args()


sys.path.append("src")


def parse_time(time: str, short=False) -> str:
    iso_time = isoparse(time)
    if short:
        new_time = f"{iso_time.year}/{iso_time.month}/{iso_time.day}"
    else:
        new_time = f"{iso_time.year}/{iso_time.month}/{iso_time.day} {iso_time.hour}:{iso_time.minute}"
    return new_time

def convert_interfaces(interfaces: list)-> list:
    conv_interfaces = []
    for el in interfaces:
        new_el = copy(el)
        new_el["last_upd"] = parse_time(el["last_upd"])
        conv_interfaces.append(new_el)
    return conv_interfaces
    

app = Flask(__name__, static_url_path=f"/{args.mount_point}")
app.url_rule_class = lambda path, **options: Rule(f"/{args.mount_point}" + path, **options)
app.config.update({
    'SECRET_KEY': 'SomethingNotEntirelySecret',
    'TESTING': False,
    'DEBUG': False,
    'OIDC_CLIENT_SECRETS': 'data/client_secrets.json',
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': 'efpf',
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post'
})

oidc = OpenIDConnect(app)

# @app.route("/")
# def hello():
#     return render_template("index.html", interfaces=None, pagination=None)

@app.route("/")
@app.route("/list")
@oidc.require_login
def list_interfaces():
    #     {
    #         "id": "lin_dlt_pp",
    #         "tool": "DLT for Price Prediction",
    #         "owner": "LINKS",
    #         "last_upd": "15/08/2020",
    #         "type_upd": "patch"
    #     }

    PER_PAGE = 15
    total = get(f"http://{args.api_host}/list/ic/count", verify=False).json()["total"]
    page = request.args.get(get_page_parameter(), type=int, default=1)
    pagination = Pagination(page=page, total=total, per_page=PER_PAGE, search=False, record_name='interfaces', css_framework='bootstrap4')

    interfaces = get(f"http://{args.api_host}/list/ic/?page={page}", verify=False).json()
    parsed_interfaces = []
    for ic in interfaces:
        ic["last_upd"] = parse_time(ic["last_upd"], short=True)
        parsed_interfaces.append(ic)
    return render_template("list.html", mount_point=args.mount_point, interfaces=parsed_interfaces, pagination=pagination)


@app.route("/list/<string:interface_id>")
@oidc.require_login
def show_interface(interface_id :str):
    interface = get(f"http://{args.api_host}/ic/?id={interface_id}", verify=False).json()
    docs = get(f"http://{args.api_host}/doc/?id={interface_id}", verify=False).json()
    # {
    #     "description": "DLT for Price Prediction",
    #     "owner": "LINKS",
    #     "email": "edoardo.pristeri@linksfoundation.com",
    #     "created": "01/01/2020",
    #     "updated": "15/08/2020",
    #     "expires": "31/12/2020",
    #     "docs": [
    #         {
    #             "description": "RESTful API Specs",
    #             "type": "OpenAPI 3.0",
    #             "url": "https://efpf.polito.it/docs/openapi_30_dlt_pp.json"
    #         },
    #         {
    #             "description": "Deep Learning Toolkit for Price Prediction Documentation",
    #             "type": "HTML",
    #             "url": "https://docs.efpf.linksmart.eu/docs/deep-learning-toolkit-pp"
    #         }
    #     ]
    # }
    interface["updated"] = parse_time(interface["updated"])
    interface["expires"] = parse_time(interface["expires"])
    interface["created"] = parse_time(interface["created"])
    if docs["old_content"]:
        changelog = escape(calc_changelog(docs["old_content"], docs["content"]))
    else:
        changelog = None
    return render_template("item.html", interface=interface, docs=docs, changelog=changelog)


@app.route("/help")
@oidc.require_login
def help():
    return render_template("help.html", interfaces=None, pagination=None)
