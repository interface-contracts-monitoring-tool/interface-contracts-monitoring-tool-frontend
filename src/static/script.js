$(function () {
    var pageUrl = location.pathname.split('/')[1];
    $('.nav-item').each(function () {
        var navUrl = $(this).children().attr("href").split('/')[1];
        if (navUrl == pageUrl){
            $(this).addClass('active');
        }
    })
})