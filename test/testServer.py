from flask import Flask, Response
import json

from jinja2.utils import internal_code

app = Flask(__name__)

@app.route("/allInterfaces", methods=["GET"])
def allInterfaces():
    interfaces = None
    with open("./test/testInterfaces.json", "r") as fp:
        interfaces = json.load(fp)
    return Response(json.dumps(interfaces), content_type="application/json")


@app.route("/interface/<string:interfaceId>", methods=["GET"])
def singleInterface(interfaceID :str):
    interface = None
    with open("./test/testInterfaces.json", "r") as fp:
        interfaces = json.load(fp)
        for inter in interfaces:
            if inter["id"] == interfaceID:
                interface = inter
            else:
                continue
    return Response(json.dumps(interface), content_type="application/json")


if __name__ == "__main__":
    app.run(host="127.0.0.1", port="8082")